<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tag extends Model
{
    use HasFactory;

    public $fillable=['name'];

    public function photo() 
    {
        return $this->belongsToMany('App\Models\Photo', 'photo_id', 'id');
    }

    public function photos()
    {
        return $this->belongsToMany('App\Models\Photo')
                    ->using("App\Models\PhotoTag")
                    ->withPivot("id")
                    ->withTimestamps();
    }

}
