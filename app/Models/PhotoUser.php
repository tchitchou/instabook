<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class PhotoUser extends Pivot
{
    use HasFactory;

    public $incrementing = true;

    public $fillable=['user_id','photo_id'];

    public function photos() 
    {
        return $this->belongsTo('App\Models\Photo');
    }

    public function users() 
    {
        return $this->belongsTo('App\Models\User');
    }
}
