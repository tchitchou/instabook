<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class GroupUser extends Pivot
{
    use HasFactory;

    public $incrementing = true;

    public $fillable=['user_id','group_id'];

    public function users() 
    {
        return $this->belongsTo('App\Models\User');
    }

    public function groups() 
    {
        return $this->belongsTo('App\Models\Group');
    }
}
