<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class PhotoTag extends Pivot
{
    use HasFactory;

    public $incrementing = true;

    public $fillable=['user_id','tag_id'];

    public function photos() 
    {
        return $this->belongsTo('App\Models\Photo');
    }

    public function tags() 
    {
        return $this->belongsTo('App\Models\Tag');
    }
}
