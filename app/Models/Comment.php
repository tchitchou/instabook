<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    public $fillable=['text'];

    public function photo() 
    {
        return $this->belongsTo('App\Models\Photo');
    }

    public function user() 
    {
        return $this->belongsTo('App\Models\User');
    }

    public function replies() 
    {
        return $this->hasMany('App\Models\Comment');
    }

    public function replyTo() 
    {
        return $this->belongsTo('App\Models\Comment', 'comment_id', 'id');
    }
}
