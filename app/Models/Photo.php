<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    use HasFactory;

    public $fillable=['title', 'description', 'file', 'data', 'resolution', 'width', 'height'];
  

    public function comments() {
        return $this->hasMany(Comment::class);
    }

    public function group() 
    {
        return $this->belongsTo('App\Models\Group');
    }

    public function owner() 
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function tag() 
    {
        return $this->belongsToMany('App\Models\Tag', 'tah_id', 'id');
    }

    public function user() 
    {
        return $this->belongsToMany('App\Models\User', 'user_id', 'id');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag')
                    ->using("App\Models\PhotoTag")
                    ->withPivot("id")
                    ->withTimestamps();
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User')
                    ->using("App\Models\PhotoUser")
                    ->withPivot("id")
                    ->withTimestamps();
    }

}
 