<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;

    public $fillable=['name', 'description'];

    public function photos() 
    {
        return $this->hasMany('App\Models\Photo');
    }

    public function user() 
    {
        return $this->belongsToMany('App\Models\User', 'user_id', 'id');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User')
                    ->using("App\Models\GroupUser")
                    ->withPivot("id")
                    ->withTimestamps();
    }
}
