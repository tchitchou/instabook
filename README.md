
Nous avons rencontré des problèmes au niveau de l’ordre des migrations. En effet, un ordre doit être établi afin que le programme exécute pas à pas les tests. De ce fait, nous avons modifié les dates des tables.

Il y a eu des problèmes de nommage de fonctions dans les modèles. Il a fallu renommer certaines fonctions ou bien rajouter 'user_id', 'id' après le chemin.

Nous avons eu un autre problème sur les modèles mais cette fois-ci il concerne une ligne importante à mettre pour les tables pivot. Il s’agit de « public $incrementing =true ». 

Pour conclure, c'est un framework qui, d'un côté est très intéressant car il nous mâche un peu le travail et facilite certaines manipulations, mais d'un autre côté, il est très difficile de l'appréhender, surtout en si peu de temps, et encore plus lorsque nous n'allons pas nous diriger vers le web, la motivation est très faible.

